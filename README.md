Olá candidato(a), tudo bem? Espero que sim…

Nosso teste demanda criar um fluxo de um restaurante com telas simples, disponíveis neste documento Figma: 
[Link do documento](https://www.figma.com/file/cEzyQEru17XXE6ADsT52bE/Desafio-FrontEnd?node-id=0%3A1)

**Você deve:**

\- Utilizar Nuxt.js;

\- Desenvolver telas responsivas com Flexbox/Grid layout;

\- Utilizar SASS;

\- Seguir corretamente as propriedades do documento Figma;

\- Permitir navegar entre as telas através dos botões destinados.

**Instruções:**

\- No cardápio deve ser permitido excluir e editar itens;

\- Os itens da lista devem ser ordenados em ordem alfabética;

\- A lista deve atualizar automaticamente sua quantidade de itens;

\- Fique à vontade para pequenas adaptações CSS, se julgar necessário;

\- Fique à vontade para implementar serviços e outras estratégias que você usa no dia a dia, desde que não adicione complexidade desnecessária ao projeto.

**Seria interessante se você utilizasse:**

\- Reaproveitamento de CSS;

\- Utilizar estrutura BEM;

**Você NÃO deve utilizar:**

\- Framework UI (ex.: Bootstrap, Vuetify, Element, Materialize, Tailwind…).

**Você será avaliado por:**

\- Estrutura do projeto;

\- Boas práticas de código;

\- Versionamento de código.

**Entrega:**

(Alinhar instruções de entrega com Renan)


